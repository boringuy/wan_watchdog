#!/bin/sh
wan_watchdog_dir=/jffs/scripts
last_reboot_file=$wan_watchdog_dir/last_reboot
reboot_delay=600 # don't reboot if we already reboot within the last 10 minutes

ping www.yahoo.com -W 5 -c 1
result=$?
if [ -e $last_reboot_file ]; then
    last_reboot=$(cat $last_reboot_file)
else
    last_reboot=0
fi
# echo $last_reboot
now=`date +%s`
since_last_reboot=`expr $now - $last_reboot`
# echo $since_last_reboot

if [ $result -ne "0" ];
then
    delay_for=`expr $reboot_delay - $since_last_reboot`
    echo "delay_for $delay_for"
    if [ $delay_for -gt 0 ]; then 
#        echo "delaying for $delay_for"
        logger -t wan_watchdog "WAN still down after restart. Delaying for $delay_for seconds!"
        exit 0
    fi
#    echo "restarting!"
    logger -t wan_watchdog "WAN down. Restarting!"
    date +%s > $last_reboot_file
    service restart_wan
fi

