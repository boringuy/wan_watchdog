A wan watch dog script to monitor internet connection and restart the wan service on a ASUS router with the Merlin Firmware.

Refer to https://github.com/RMerl/asuswrt-merlin/wiki/User-scripts to add custom user script to the router.

Put all the files in /jffs/scripts.

Warning: if your services-start file is not empty, please append to the file instead
